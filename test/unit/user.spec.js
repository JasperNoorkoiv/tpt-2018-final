const user = require('../..src/user');

it('renders correctly', () => {
    expect(user(1)).toMatchSnapshot();
    expect(user(56)).toMatchSnapshot();
    expect(user(1345)).toMatchSnapshot();
});

it('string input', () => {
    expect(() => {
        user("absdf");
    }).toThrowError('id needs to be integer');
});