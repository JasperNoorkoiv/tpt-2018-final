var config = require('../../nightwatch.conf.js');

module.exports = {
    'tallinn test': function(browser) {
        browser
        .url('http://google.com')
        .waitForElementVisible('body div#main', 1000)
        .setValue('input[type=text]', 'Tallinn')
        .pause(1000)
        .click('input[name=btnK]')
        .pause(1000)
        .assert.containsText('.bkWMgd:first-child', 'Tallinn')
        .saveScreenshot(config.imgpath(browser) + 'tallinngoogle.png')
        .click('.LC20lb:first-child')
        .pause(2000)
        .saveScreenshot(config.imgpath(browser) + 'tallinnresult.ng')
        .end();

    }
};
