var config = require('../../nightwatch.conf.js');

module.exports = {
    'tpt test': function(browser) {
        browser
        .url('http://www.tptlive.ee')
        .waitForElementVisible('#main-menu-wrapper', 1000)
        .saveScreenshot(config.imgpath(browser) + 'tpt.png')
        .click('#menu-item-1313')
        .pause(1000)
        .saveScreenshot(config.imgpath(browser) + 'tunniplaantpt.png')
        .useXpath()
        .click("//a[text()='TA-17E']")
        .useCss()
        .waitForElementVisible('body')
        .saveScreenshot(config.imgpath(browser) + 'tal17tpt.png')
        .pause(3000)
        .end();

    }
};
